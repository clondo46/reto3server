import os
from flask import Flask, flash, request, redirect, url_for, render_template
from werkzeug.utils import secure_filename
import cv2
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.svm import SVC
from collections import Counter
from sklearn.externals import joblib
#from flask.ext.session import Session

UPLOAD_FOLDER = 'static/'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
#sess = Session()
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    places = {1: "38",2:"agora",3:"26",4:"biblioteca",5:"admisiones", 6:"dogger",7:"auditorio",8:"19",9:"18",10:"Centro de idiomas"}
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
	    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
	    img = cv2.imread("static/"+filename)
	    #print(img)
	    img = cv2.resize(img, (250, 250))
            gray= cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
            sift = cv2.xfeatures2d.SIFT_create()
            kp, des = sift.detectAndCompute(gray,None)
	    arrayFin = [0]*60
	    kmeans = joblib.load("kmeans.pkl")
	    predict = kmeans.predict(des)
            a = Counter(predict)
	    for keys in a.keys():
		arrayFin[keys] = a[keys]
	    arrayValues = np.array(arrayFin,dtype=float)
	    maxValue = max(arrayFin)
	    aF = arrayValues/maxValue
	    clf = joblib.load("modelBueno.pkl")
            p = []
	    p.append(aF)
	    print("HOLA AMIGO SI TU YO CREO QUE ESTOY ACA: " + str(places[clf.predict(p)[0]]))   
            return render_template("imagen.html", filename=filename, lugar=str(places[clf.predict(p)[0]]))

    return '''
        <!DOCTYPE html>
    <html lang="en">
    <head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    </head>
    <style>
        body{
            background-color: pink;
        }
    </style>
    <body>

        <div class="container" >
            <div class="row">
            <div class="col-sm-4" style="background-color:solid pink"> 
                <h3>Analizador de lugares EAFIT</h3>
                <form method=post enctype=multipart/form-data>
                    <input type=file name=file>
                    <input type=submit value=Analizar >
                    <p>Elije o toma foto y dale a Analizar</p>
                </form>               
        </div>
    </body>
    '''
if __name__ == "__main__":
	#app.secret_key = 'super secret key'
	#app.config['SESSION_TYPE'] = 'filesystem'

	#sess.init_app(app)
        app.run(host='0.0.0.0', debug=True)
